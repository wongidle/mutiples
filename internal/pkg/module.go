package pkg

import (
	"gitlab.com/wongidle/mutiples/v2/internal/pkg/httpsrv"
	"gitlab.com/wongidle/mutiples/v2/internal/pkg/mysql"
	"go.uber.org/fx"
)

var Module = fx.Module(
	"internal.pkg",
	fx.Provide(httpsrv.NewHTTPServer),
	fx.Provide(mysql.NewMySQLDriver),
)
