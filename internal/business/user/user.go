package user

import (
	"gitlab.com/wongidle/mutiples/v2/internal/business/user/application"
	"gitlab.com/wongidle/mutiples/v2/internal/business/user/infrastructure"
	"gitlab.com/wongidle/mutiples/v2/internal/business/user/transport"
	"gitlab.com/wongidle/mutiples/v2/internal/pkg/httpsrv"
	"go.uber.org/fx"
)

var Module = fx.Module(
	"domain.user",
	fx.Provide(infrastructure.NewQueryRepository),
	fx.Provide(transport.NewController),
	fx.Provide(application.NewApplication),
	fx.Provide(infrastructure.NewRepository),
	fx.Invoke(func(srv httpsrv.HTTPServer, controller httpsrv.Controller) {
		srv.With(controller)
	}),
)
