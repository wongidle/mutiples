package application

import (
	"github.com/jinzhu/copier"
	"gitlab.com/wongidle/mutiples/v2/internal/business/user/domain"
)

func assembleDomainUser(entity *domain.User) (*User, error) {
	du := new(User)
	if err := copier.Copy(du, entity); err != nil {
		return nil, err
	}
	return du, nil
}
